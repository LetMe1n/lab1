﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Lab1
{
    class XmlSerializer<T> : ISerializer<T>
    {
        protected System.Xml.Serialization.XmlSerializer serializer;

        public XmlSerializer()
        {
            XmlAttributeOverrides attributeOverrides = getAttributeOverrides();
            serializer = new System.Xml.Serialization.XmlSerializer(
                typeof(T), attributeOverrides
            );
        }

        protected XmlAttributeOverrides getAttributeOverrides()
        {
            XmlAttributes attributes = new XmlAttributes();
            XmlAttributeOverrides attributeOverrides = new XmlAttributeOverrides();
            attributes.Xmlns = false;
            attributes.XmlType = new XmlTypeAttribute() { Namespace = "" };
            attributeOverrides.Add(typeof(Output), attributes);
            return attributeOverrides;
        }

        public MemoryStream Serialize(T obj)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            MemoryStream stream = new MemoryStream();
            using (XmlWriter writer = XmlWriter.Create(stream, settings))
            {
               serializer.Serialize(writer, obj);
            }
            return stream;
        }

        public T Deserialize(MemoryStream stream)
        {
            return (T)serializer.Deserialize(stream);
        }

    }
}
