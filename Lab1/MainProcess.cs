﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class MainProcess
    {
        public void run()
        {
            
            string serializeType = Console.ReadLine().ToLower();
            try
            {
                ISerializer<Output> outputSerializer = SerializerFactory.getSerializer<Output>(serializeType);
                ISerializer<Input> inputSerializer = SerializerFactory.getSerializer<Input>(serializeType);

                string inputString = Console.ReadLine();
                MemoryStream inputStream = GenerateStreamFromString(inputString);
                Input input = inputSerializer.Deserialize(inputStream);
                // Расчет вывода
                Output output = new Output();
                output.SumResult = input.Sums.Select(x => x * input.K).Sum();
                output.MulResult = input.Muls.Aggregate((x, next) => x * next);
                var decimalMuls = input.Muls.Select(x => (decimal)x).ToArray();
                output.SortedInputs = decimalMuls.Concat(input.Sums).OrderBy(x => x).ToArray();
                // Вывод
                if (serializeType == SerializerFactory.JSON) forJson(output);
                MemoryStream outputStream = (MemoryStream) outputSerializer.Serialize(output);
                String outputString = GenerateStringFromStream(outputStream);
                Console.Write(outputString);
            } catch (Exception e)
            {
                Console.WriteLine("Ошибка: " + e.Message);
            }
        }

        protected void forJson(Output output)
        {
            for (int i = 0; i < output.SortedInputs.Count(); i++)
            {
                if (output.SortedInputs[i] - (int)output.SortedInputs[i] == 0)
                    output.SortedInputs[i] *= 1.0m;
            }
        }

        public static String GenerateStringFromStream(MemoryStream stream)
        {
            return new string(Encoding.UTF8.GetChars(stream.ToArray()));
        }

        public static MemoryStream GenerateStreamFromString(string value)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(value));
        }
    }
}
