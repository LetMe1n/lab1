﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    interface ISerializer<T>
    {
        MemoryStream Serialize(T obj);
        T Deserialize(MemoryStream stream);
    }
}
