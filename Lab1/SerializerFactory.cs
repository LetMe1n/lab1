﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class SerializerFactory
    {
        public const String XML = "xml";
        public const String JSON = "json";

        public static ISerializer<T> getSerializer<T>(String typeSerilization)
        {
            switch (typeSerilization)
            {
                case XML: return new XmlSerializer<T>();
                case JSON:return new JsonSerializer<T>();
                default: throw new Exception("Неизвестный тип сериализации");
            }
        }
    }

}
